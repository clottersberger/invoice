/**
 * Ein Rechnungs-Programm in Java.
 * Dies ist ein Javadoc-Kommentar.
 *
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 14.10.2020
 */

//Klasse
public class Invoice {

    //Eigenschaften/Attribute
    private static int date;
    private static String description;
    private static double value;
    private static Boolean paid;
    private static int id;

    //Methoden
    public static void showInvoices() {

    }

    /**
     * @param date        das Datum
     * @param description die Beschreibung
     * @param value       der Wert
     * @param paid        bezahlt (true/false)
     */
    public static void insertInvoice(int date, String description, double value, boolean paid) {

        Invoice.date = date;
        Invoice.description = description;
        Invoice.value = value;
        Invoice.paid = paid;
    }

    /**
     * @param id          ID
     * @param date        das Datum
     * @param description die Beschreibung
     * @param value       der Wert
     * @param paid        bezahlt (true/false)
     */
    public static void updateInvoice(int id, int date, String description, double value, boolean paid) {

        Invoice.id = id;
        Invoice.date = date;
        Invoice.description = description;
        Invoice.value = value;
        Invoice.paid = paid;
    }

    /**
     * @param id ID
     */
    public static void deleteInvoice(int id) {

        Invoice.id = id;
    }

}
