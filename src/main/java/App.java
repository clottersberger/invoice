/**
 * Ein Rechnungs-Programm in Java.
 * Dies ist ein Javadoc-Kommentar.
 *
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 14.10.2020
 */

//Importieren

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

//Klasse
public class App {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/Invoice", "root", "1234");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Invoice");
            while (rs.next())
                System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5));
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        //Methoden aufrufen
        Invoice.deleteInvoice(2);
        Invoice.insertInvoice(1510, "Rechnung2", 100, true);
        Invoice.showInvoices();
    }
}
